package KiK;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Random;

/**
*   @author Kacper Seweryn
*   @version 1.0
 **/

public class WisielecWybor {
    /**
     *  Glowny panel w ktorym znajduja sie wszystkie elementy typu JButton czy JTextArea
     */
    public JPanel panelName;
    /**
     *  Przycisk odpowiadajacy za wybranie opcji gry GRACZ vs PC
     */
    private JButton button3;
    /**
     *  Przycisk odpowiadajacy za wybranie opcji gry GRACZ vs GRACZ
     */
    private JButton button4;
    /**
     *  Napis wyswietlajacy prosbe o wpisanie hasla do odgadniecia
     */
    private JLabel hasloDlaPrzeciwnika;
    /**
     *  Pole, w ktorym uzytkownik wpisuje haslo do odgadniecia
     */
    public JTextArea hasloWprowadzane;
    /**
     *  Przycisk potwierdzajacy wpisanie kategorii oraz hasla
     */
    private JButton OKButton;
    /**
     *  Pole, w ktorym uzytkownik wpisuje kategorie do odgadniecia
     */
    public JTextArea kategoriaWprowadzana;
    /**
     *  Napis wyswietlajacy prosbe o wpisanie kategorii do odgadniecia
     */
    private JLabel kategoriaLabel;
    /**
     *  Napis wyswietlajacy ilosc znakow wprowadzonej kategorii
     */
    private JLabel ilZnakKat;
    /**
     *  Napis wyswietlajacy ilosc znakow wprowadzonego hasla
     */
    private JLabel ilZnakHaslo;
    /**
     *  Przycisk odpowiedzialny za powrot do STARTu z poziomu wyboru opcji gry WISIELEC
     */
    private JButton buttonBack;
    private JButton sdsaButton;
    /**
     *  Zmienna przechowujaca ilosc znakow hasla
     */
    private int dlHasla;
    /**
     *  Zmienna przechowujaca ilosc znakow kategorii
     */
    private int dlKat;
    /**
     *  Zmienna przechowujaca dane o wybranej opcji przez gracza (czy wybrana opcja to Gracz vs PC czy Gracz vs Gracz)
     */
    public boolean czyGraZKomputerem;
    /**
     *  Zmienna przechowujaca haslo w postaci String
     */
    public static String haslo="X";
    /**
     *  Zmienna przechowujaca kategorie w postaci String
     */
    public static String kategoria="X";
    /**
     *  statyczny obiekt gra utworzony w celu uruchomienia wlasciwej planszy z gra
     */
    public static Wisielec gra = new Wisielec();
    String utnij="X";

    /**
     *  Konstruktor przypisujacy dane oraz parametry startowe poszczegolnych obiektow.
     */
    public WisielecWybor() {
        hasloWprowadzane.setText("");
        hasloWprowadzane.setWrapStyleWord(true);
        kategoriaWprowadzana.setText("");
        kategoriaWprowadzana.setWrapStyleWord(true);

        button3.setBorderPainted(false);
        button4.setBorderPainted(false);
//GRA Z KOMPEM
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                czyGraZKomputerem=true;
                System.out.println("kliknieto gre z botem");
                DataBase db = null;
                try {
                     db = new DataBase();
                    int bound  = db.getRowIterator();
                    int rand = new Random().nextInt(bound);
                    String [][] base = db.getBase();
                    kategoria= base[rand][0];
                    haslo = base[rand][1];
                    haslo = haslo.toUpperCase();
                    gra.pokazPlansze();
                    Start.frameWisielecWybor.dispose();
                } catch (IOException e1) {
                    System.out.println("wykrzaczylo polaczenie z baza");
                    e1.printStackTrace();
                }


                //Losowanie hasla z bazy danych
            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button3.setEnabled(false);
                czyGraZKomputerem=false;
                hasloWprowadzane.setVisible(true);
                hasloDlaPrzeciwnika.setVisible(true);
                kategoriaWprowadzana.setVisible(true);
                kategoriaLabel.setVisible(true);
                OKButton.setVisible(true);
                ilZnakHaslo.setVisible(true);
                ilZnakKat.setVisible(true);
            }
        });
        OKButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                haslo=hasloWprowadzane.getText();
                haslo=haslo.toUpperCase();
                kategoria=kategoriaWprowadzana.getText();
                kategoria=kategoria.toUpperCase();
                if (dlHasla>150 || dlKat >50){
                    JOptionPane.showConfirmDialog(null,"Hasło lub kategoria są za długie!!", "Hola Hola!!", JOptionPane.CLOSED_OPTION);
                }
                else if (kategoria.equals("") || haslo.equals("")){
                    JOptionPane.showConfirmDialog(null,"Wprowadź kategorię oraz hasło!!", "Hola Hola!!", JOptionPane.CLOSED_OPTION);
                }
                else{
                    gra.pokazPlansze();
                    Start.frameWisielecWybor.dispose();
                }
            }
        });

        kategoriaWprowadzana.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                dlKat=kategoriaWprowadzana.getText().length();

                if(dlKat<41)utnij=kategoriaWprowadzana.getText();
                if(dlKat>40){
                    kategoriaWprowadzana.setText(utnij);
                    dlKat=kategoriaWprowadzana.getText().length();
                    JOptionPane.showConfirmDialog(null,"Kategoria zawiera za dużo znaków!", "Hola Hola!!", JOptionPane.CLOSED_OPTION);
                }
                ilZnakKat.setText(Integer.toString(dlKat)+"/40");
            }
        });
        hasloWprowadzane.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                dlHasla=hasloWprowadzane.getText().length();

                if(dlHasla<151)utnij=hasloWprowadzane.getText();
                if(dlHasla>150){
                    hasloWprowadzane.setText(utnij);
                    dlHasla=hasloWprowadzane.getText().length();
                    JOptionPane.showConfirmDialog(null,"Hasło zawiera za dużo znaków!", "Hola Hola!!", JOptionPane.CLOSED_OPTION);
                }
                ilZnakHaslo.setText(Integer.toString(dlHasla)+"/150");
            }

        });
        buttonBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Start powrot = new Start();
                powrot.pokazPlansze();
                Start.frameWisielecWybor.dispose();
            }
        });
    }


}

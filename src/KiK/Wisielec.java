package KiK;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *   @author Kacper Seweryn
 *   @version 1.0
 **/

public class Wisielec {
    /**
     *  Glowny panel w ktorym znajduja sie przyciski z literami do gry Wisielec
     */
    private JPanel PolePrzyciskow;
    /**
     *  Glowny panel w ktorym znajduja sie wszystkie elementy typu JButton czy JTextArea
     */
    private JPanel mainPanel;
    /**
     * Pole odpowiedzialne za wyswietlenie aktualnego hasla
     */
    private JTextArea hasloLabel;
    /**
     *  Pole, w ktorym znajduje sie odpowiedni obrazek Wisielca, zalezny od ilosci popelnionych pomylek
     */
    private JLabel wisielecImg;
    /**
     *  Przycisk odpowiedzialny za powrot do menu
     */
    private JButton buttonBack;
    /**
     *  Przycisk odpowiedzialny za zresetowanie gry i powrot do planszy, gdzie uzytkownik wybiera tryb gry
     */
    private JButton buttonReset;
    /**
     *  Pole wyswietlajace kategorie aktualnie odgadywanego hasla
     */
    private JLabel kategoriaLabel;
    /**
     *  Zmienna przechowujaca rozmiar przycisku zawierajacego dana litere
     */
    private int buttonSize=30;
    /**
     *  Zmienna przechowujaca haslo odgadywane w postaci String
     */
    private String hasloNapis="X";
    /**
     *  Tablica przechowujace pojedyncze znaki hasla
     */
    private char[] hasloTablica;
    /**
     *  Tablica przechowujace pojedyncze znaki hasla z odpowiednia iloscia odslonietych liter, konwertowana pozniej na String w celu wyswietlenia na planszy.
     */
    private char[] hasloDoWyswietlenia;
    /**
     *  Zmienna, ktora przechowuje haslo przekonwertowane z tablicy char[] hasloDoWyswietlenia
     */
    private String hasloDoWyswietleniaString="";
    /**
     *  Zmienna przechowujaca ilosc znakow hasla
     */
    private int dlugoscHasla;
    /**
     *  Zmienna przechowujaca ilosc pomylek popelnionych przez gracza
     */
    private int iloscPomylek;
    /**
     *  Zmienna przechowujaca sciezke do wyswietlanego aktualnie obrazku Wisielca
     */
    private String sciezkaDoImg;
    /**
     *  Tablica przechowujaca przyciski z literami
     */
    private  JButton[] tablicaPrzyciskow;


    //konstruktor
    /**
     *  Konstruktor umozliwiajacy przypisanie danych startowych atrybutow gry Wisielec
     */
    public Wisielec(){
        Container ButtonsPanel;
        ButtonsPanel = PolePrzyciskow;
        ButtonsPanel.setLayout(new GridLayout(5,8));
        ButtonsPanel.setMaximumSize(new Dimension(500,400));
        tablicaPrzyciskow=new JButton[35];
        mainPanel.setMinimumSize(new Dimension(1200,700));
        mainPanel.setMaximumSize(new Dimension(1200,700));
        mainPanel.setPreferredSize(new Dimension(1200,700));
        //hasloLabel.setMaximumSize(new Dimension(500,200));
        hasloLabel.setMinimumSize(new Dimension(500,100));
        hasloLabel.setPreferredSize(new Dimension(500,100));
        hasloLabel.setLineWrap(true);
        hasloLabel.setWrapStyleWord(true);
        hasloLabel.setEditable(false);
        kategoriaLabel.setText(WisielecWybor.kategoria);
        Color kolor=mainPanel.getBackground();
        hasloLabel.setBackground(kolor);
        hasloLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        hasloLabel.setAlignmentY(Component.CENTER_ALIGNMENT);


        for (int i=0; i<35; i++){
            final JButton button = new JButton();

            //65 A
            button.setName(Integer.toString(i));
            button.setMaximumSize(new Dimension(buttonSize, buttonSize));
            button.setMaximumSize(new Dimension(buttonSize, buttonSize));
            button.setFocusPainted(false);
            button.setFont(new Font("Impact", Font.PLAIN, 30));

            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    sprawdzPrzycisk(button);
                }
            });
            PolePrzyciskow.add(button);
            tablicaPrzyciskow[i]=button;

        }
        tablicaPrzyciskow[0].setText("A");
        tablicaPrzyciskow[1].setText("Ą");
        tablicaPrzyciskow[2].setText("B");
        tablicaPrzyciskow[3].setText("C");
        tablicaPrzyciskow[4].setText("Ć");
        tablicaPrzyciskow[5].setText("D");
        tablicaPrzyciskow[6].setText("E");
        tablicaPrzyciskow[8].setText("F");
        tablicaPrzyciskow[9].setText("G");
        tablicaPrzyciskow[10].setText("H");
        tablicaPrzyciskow[11].setText("I");
        tablicaPrzyciskow[12].setText("J");
        tablicaPrzyciskow[13].setText("K");
        tablicaPrzyciskow[14].setText("L");
        tablicaPrzyciskow[16].setText("M");
        tablicaPrzyciskow[17].setText("N");
        tablicaPrzyciskow[19].setText("O");
        tablicaPrzyciskow[20].setText("Ó");
        tablicaPrzyciskow[21].setText("P");
        tablicaPrzyciskow[22].setText("Q");
        tablicaPrzyciskow[23].setText("R");
        tablicaPrzyciskow[24].setText("S");
        tablicaPrzyciskow[25].setText("Ś");
        tablicaPrzyciskow[26].setText("T");
        tablicaPrzyciskow[27].setText("U");
        tablicaPrzyciskow[28].setText("V");
        tablicaPrzyciskow[29].setText("W");
        tablicaPrzyciskow[30].setText("X");
        tablicaPrzyciskow[31].setText("Y");
        tablicaPrzyciskow[32].setText("Z");
        tablicaPrzyciskow[33].setText("Ź");
        tablicaPrzyciskow[34].setText("Ż");

        tablicaPrzyciskow[18].setText("Ń");
        tablicaPrzyciskow[15].setText("Ł");
        tablicaPrzyciskow[7].setText("Ę");

        generowanieHasla();
        buttonBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Start powrot = new Start();
                powrot.pokazPlansze();
                Start.frameWisielecGra.dispose();
            }
        });
        buttonReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Start.frameWisielecGra.dispose();
                Start.PokazWisielecWybor();
            }
        });
    }
    /**
     *  Funkcja odpowiedzialna za przetwarzanie hasla podanego przez uzytkownika oraz przekonwertowanie go na tablice znakow char[]
     */
    private void generowanieHasla(){
        hasloNapis=WisielecWybor.haslo;
        dlugoscHasla=hasloNapis.length();
        if (dlugoscHasla<50) hasloLabel.setFont(new Font("Impact", Font.PLAIN, 48));
        else if (dlugoscHasla<100) hasloLabel.setFont(new Font("Impact", Font.PLAIN, 36));
        else hasloLabel.setFont(new Font("Impact", Font.PLAIN, 26));
        //System.out.println(hasloNapis);
        hasloTablica = new char[dlugoscHasla];
        hasloDoWyswietlenia = new char[dlugoscHasla];
        hasloTablica=hasloNapis.toCharArray();
        hasloDoWyswietleniaString="";
        for(int i=0; i<dlugoscHasla; i++){
            if(hasloTablica[i]==' '){
                hasloDoWyswietlenia[i]=' ';
            }
            else if ((hasloTablica[i]>64 && hasloTablica[i]<91) || (hasloTablica[i]==260 || hasloTablica[i]==262 || hasloTablica[i]==280 || hasloTablica[i]==321 || hasloTablica[i]==323 || hasloTablica[i]==211 || hasloTablica[i]==346 || hasloTablica[i]==377 || hasloTablica[i]==379)){
                hasloDoWyswietlenia[i]='_';
            }
            else {
                hasloDoWyswietlenia[i]=hasloTablica[i];
            }
        }
        obslugaWyswietlaniaHasla();

    }
    /**
     *  Funkcja odpowiedzialna za obsluge wyswietlania aktualnego stanu odgadywanego hasla z odpowiednia iloscia odslonietych liter
     */
    private void obslugaWyswietlaniaHasla(){
        for(int i=0; i<dlugoscHasla; i++){
            if(hasloDoWyswietlenia[i]=='_'){
                hasloDoWyswietleniaString=hasloDoWyswietleniaString+hasloDoWyswietlenia[i]+" ";
            }
            else if(hasloDoWyswietlenia[i]==' ')
                hasloDoWyswietleniaString=hasloDoWyswietleniaString+hasloDoWyswietlenia[i]+"     ";
            else hasloDoWyswietleniaString=hasloDoWyswietleniaString+hasloDoWyswietlenia[i];
        }
        hasloLabel.setText(hasloDoWyswietleniaString);
    }
    /**
     *  Funkcja odpowiedzialna za sprawdzenie, czy klikniety przycisk zawiera litere, ktora znajduje sie w odgadywanym hasle
     */
    private void sprawdzPrzycisk(JButton przycisk){
        char literaSprawdzana[]=przycisk.getText().toCharArray();
        char literaSprawdzana1=literaSprawdzana[0];
        //System.out.println(literaSprawdzana1);
        boolean czyTrafil=false;
        hasloDoWyswietleniaString="";
        for(int i=0; i<dlugoscHasla; i++){
            if (hasloTablica[i]==literaSprawdzana1){
                hasloDoWyswietlenia[i] = literaSprawdzana1;
                czyTrafil=true;
            }
        }
        obslugaWyswietlaniaHasla();
        if(czyTrafil){
            przycisk.setBackground(Color.GREEN);
            przycisk.setEnabled(false);
        }
        else {
            iloscPomylek++;
            sciezkaDoImg="/wisielec/";
            sciezkaDoImg=sciezkaDoImg+Integer.toString(iloscPomylek)+".png";
            wisielecImg.setIcon(new ImageIcon(Class.class.getResource(sciezkaDoImg)));
            przycisk.setBackground(Color.RED);
            przycisk.setEnabled(false);
        }
        sprawdzWygrana();
    }
    /**
     *  Funkcja odpowiedzialna za sprawdzenie wygranej lub przegranej oraz wyswietlenie stosownego komunikatu.
     */
    private void sprawdzWygrana(){
        if(iloscPomylek>7) {
            JOptionPane.showConfirmDialog(null, "Niestety nie odgadłeś hasła. Nic straconego, spróbuj ponownie :-)", "Przegrana :-(", JOptionPane.CLOSED_OPTION);
            for (int i = 0; i < 35; i++) {
                tablicaPrzyciskow[i].setEnabled(false);
            }
            hasloLabel.setText(hasloNapis);
            hasloLabel.setForeground(Color.RED);
        }
            boolean czyZgodneHaslo=true;
            for(int i=0; i<dlugoscHasla; i++){
                if(hasloTablica[i]!=hasloDoWyswietlenia[i])czyZgodneHaslo=false;
            }
            if(czyZgodneHaslo){
                for (int i = 0; i < 35; i++) {
                    tablicaPrzyciskow[i].setEnabled(false);
                }
                hasloLabel.setForeground(Color.RED);
                wisielecImg.setIcon(new ImageIcon(Class.class.getResource("/wisielec/wygrana.png")));
                JOptionPane.showConfirmDialog(null, "WYGRANA!!! Udało Ci się odgadnąć hasło :-)", "Gratulacje!!!", JOptionPane.CLOSED_OPTION);
            }
    }
    /**
     *  Funkcja odpowiedzialna za pokazanie planszy gry Wisielec
     */
    public void pokazPlansze(){
        Start.frameWisielecGra.setContentPane(new Wisielec().mainPanel);
        Start.frameWisielecGra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Start.frameWisielecGra.pack();
        Start.frameWisielecGra.setVisible(true);
        Start.frameWisielecGra.setLocationRelativeTo(null);
        Start.frameWisielecGra.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

}

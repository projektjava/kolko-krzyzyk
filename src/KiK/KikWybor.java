package KiK;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class KikWybor {
    /**
     *  Przycisk opcji 1 odpowiedzialny wraz z button2  za wybor znaku rozpoczynajacego. (O/X)
     */
    private JButton button1;//kolko
    /**
     *  Przycisk opcji 1 odpowiedzialny wraz z button1  za wybor znaku rozpoczynajacego. (O/X)
     */
    private JButton button2;//krzyzyk
    /**
     *  Przycisk opcji 2 odpowiedzialny wraz z button4  za tryb gry. (PvP/PvC)
     */
    private JButton button3;//pvc
    /**
     *  Przycisk opcji 2 odpowiedzialny wraz z button3  za tryb gry. (PvP/PvC)
     */
    private JButton button4;//pvp
    /**
     *  Przycisk opcji 3 pojawiajacy sie po wybraniu trybu gry PvC odpowiedzialny wraz z button6 za wybor rozpoczynajacego rozgrywke. (Gracz/Komputer)
     */
    private JButton button5;//gracz
    /**
     *  Przycisk opcji 3 pojawiajacy sie po wybraniu trybu gry PvC odpowiedzialny wraz z button5 za wybor rozpoczynajacego rozgrywke. (Gracz/Komputer)
     */
    private JButton button6;//komputer
    /**
     *  Glowny panel w ktorym znajduja sie wszystkie elementy typu JButton czy JLabel
     */
    private JPanel panelName;
    /**
     *  Napis wyswietlajacy zapytanie kto rozpoczyna (Gracz/Komputer)
     */
    private JLabel napisKtoRozpoczyna;
    /**
     *  Przycisk odpowiedzialny za powrot do STARTu z poziomu wyboru opcji gry KIK
     */
    private JButton buttonBack;
    private JButton sdsaButton;
    /**
     *  Zmienna przechowujaca znak gracza rozpoczynajacego, ktora zmienia swoja wartosc po wybraniu button1/button2
     */
    public static String znak_gracza="n"; //znak gracza rozpoczynajacego
    /**
     *  Zmienna przechowujaca informacje czy uzytkownik wybral znak gracza rozpoczynajacego
     */
    public boolean czyWybranoZnak=false;
    /**
     *  Zmienna przechowujaca informacje o trybie gry (PvP/PvC).
     */
    public static String jaka_gra="n";//zmienna przechowuje dane czy gra pvp czy pvc
    /**
     *  Zmienna przechowujaca informacje czy uzytkownik wybral tryb gry
     */
    public boolean czyWybranoTrybGry=false;
    /**
     *  Zmienna przechowujaca informacje o rozpoczynajacym (gracz/komputer)
     */
    public static String kto_zaczyna="nic"; // gracz czy komputer? pod warunkiem ze wczesniej gracz wybierze gre pvc
    /**
     *  Zmienna przechowujaca informacje czy uzytkownik wybral rozpoczynajacego
     */
    public boolean czyWybranoKtoZaczyna=false;
    /**
     *  utowrzenie frame`a KikWybor
     */
    JFrame frame = new JFrame();

    /**
     *  Funkcja sprawdza czy wszystkie opcje, ktore uzytkownik powinien wypelnic sa juz uzupelnione.
     */
    private void sprawdzCzyWszystko(){

        if(czyWybranoKtoZaczyna && czyWybranoTrybGry && czyWybranoZnak){
            Start.KikGra.pokazPlansze();
        }
        else if(czyWybranoTrybGry && czyWybranoZnak && !czyWybranoKtoZaczyna){
            if(znak_gracza!="nic" && jaka_gra=="pvp"){
                Start.KikGra.pokazPlansze();
            }
        }
    }

    /**
     *  Konstruktor przypisujacy dane oraz parametry startowe poszczegolnych obiektow takie jak np. ActionListenery, widocznosc i wiele innych.
     */
    public KikWybor() {
        button5.setEnabled(false);
        button6.setEnabled(false);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                znak_gracza="o";
                czyWybranoZnak=true;
                button2.setEnabled(false);
                sprawdzCzyWszystko();
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                znak_gracza="x";
                czyWybranoZnak=true;
                button1.setEnabled(false);
                sprawdzCzyWszystko();
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jaka_gra="pvc";
                czyWybranoTrybGry=true;
                button5.setEnabled(true);
                button6.setEnabled(true);
                button4.setEnabled(false);
                sprawdzCzyWszystko();

            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jaka_gra="pvp";
                czyWybranoTrybGry=true;
                kto_zaczyna="g";
                button3.setEnabled(false);
                sprawdzCzyWszystko();

            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                kto_zaczyna="g";
                czyWybranoKtoZaczyna=true;
                button6.setEnabled(false);
                sprawdzCzyWszystko();
            }
        });
        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                kto_zaczyna="k";
                czyWybranoKtoZaczyna=true;
                button5.setEnabled(false);
                sprawdzCzyWszystko();
            }
        });


        buttonBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Start powrot = new Start();
                powrot.pokazPlansze();
                App.zapytanie.frame.dispose();
            }
        });
    }

    /**
     *  Funckja odpowiedzialna za przypisanie parametrow JFrame oraz wyswietlenie ramki na srodku ekranu
     */
    public void pokazOkno(){
        frame.setContentPane(new KikWybor().panelName);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setMinimumSize(new Dimension(800,660));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
        Point newLocation = new Point(middle.x - (frame.getWidth() / 2),
                middle.y - (frame.getHeight() / 2));
        frame.setLocation(newLocation);
    }
    /**
     *  Funckja odpowiedzialna za ukrycie glownej rozgrywki Kolko i krzyzyk a nastepnie za pokazanie okna wyboru opcji do gry Kolko Krzyzyk
     */
    public void pokazOknoReset(){
        Start.KikGra.frameApp.dispose();
        frame.setContentPane(new KikWybor().panelName);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setMinimumSize(new Dimension(800,660));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
        Point newLocation = new Point(middle.x - (frame.getWidth() / 2),
                middle.y - (frame.getHeight() / 2));
        frame.setLocation(newLocation);
    }

}

package KiK;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
/**
 *   @author Kacper Seweryn
 *   @version 1.0
 **/


public class App{
    /**
     *  Glowny panel w ktorym znajduja sie wszystkie elementy typu JButton czy JTextArea
     */
    private JPanel panelName;
    /**
     *  Przycisk odpowiadajacy za wybranie przez gracza pola, na ktorym chce postawic swoj ruch.
     */
    private JButton button1;
    /**
     *  Przycisk odpowiadajacy za wybranie przez gracza pola, na ktorym chce postawic swoj ruch.
     */
    private JButton button4;
    /**
     *  Przycisk odpowiadajacy za wybranie przez gracza pola, na ktorym chce postawic swoj ruch.
     */
    private JButton button7;
    /**
     *  Przycisk odpowiadajacy za wybranie przez gracza pola, na ktorym chce postawic swoj ruch.
     */
    private JButton button2;
    /**
     *  Przycisk odpowiadajacy za wybranie przez gracza pola, na ktorym chce postawic swoj ruch.
     */
    private JButton button5;
    /**
     *  Przycisk odpowiadajacy za wybranie przez gracza pola, na ktorym chce postawic swoj ruch.
     */
    private JButton button8;
    /**
     *  Przycisk odpowiadajacy za wybranie przez gracza pola, na ktorym chce postawic swoj ruch.
     */
    private JButton button3;
    /**
     *  Przycisk odpowiadajacy za wybranie przez gracza pola, na ktorym chce postawic swoj ruch.
     */
    private JButton button6;
    /**
     *  Przycisk odpowiadajacy za wybranie przez gracza pola, na ktorym chce postawic swoj ruch.
     */
    private JButton button9;
    /**
     *  Przycisk odpowiadajacy za powrot do MENU.
     */
    private JButton buttonBack;
    /**
     *  Przycisk odpowiadajacy za zresetowanie gry i wyswietlenie planszy wyboru kto rozpoczyna gre.
     */
    private JButton buttonReset;
    /**
     *  Przycisk odpowiadajacy za wyswietlanie znaku gracza
     */
    private JButton buttonTura;
    /**
     *  Napis wyswietlajacy informacje o znaku gracza, ktory rozgrywa w danej turze.
     */
    private JLabel turaNapis;
    /**
     *  Kontener, w ktorym znajduja sie przyciski
     */
    private JPanel przyciskiPanel;
    /**
     *  Zmienna przechowujaca informacje, czy nastapila juz wygrana.
     */
    private boolean czyWygrana=false;
    /**
     *  Zmienna przechowujaca informacje, czy aktualnie prowadzona rozgrywka odbywa sie z komputerem czy z drugim graczem.
     */
    public static boolean czygrazkomputerem;
    /**
     *  Zmienna przechowujaca informacje, ile ruchow wystapilo od poczatku rozgrywki.
     */
    private int iloscruchow=0;
    /**
     *  Tablica zawierajaca 9 przyciskow odpowiedzialnych za postawienie ruchu na planszy.
     */
    JButton tablicaprzyciskow[]=new JButton[10];
    /**
     *  Zmienna przechowujaca informacje, jaki znak rozgrywa w danej turze.
     */
    public static String ktogra;
    /**
     *  Tablica reprezentujaca aktualna sytuacje na planszy odnosnie postawionych ruchow przez graczy/komputer.
     */
    public String tab[]=new String[10];
    /**
     *  Tablica zawierajaca informacje czy dane pole zostalo juz wczesniej wybrane przez gracza/komputer.
     */
    int czyklikniety[]=new int[10];
    /**
     *  Statyczny obiekt klasy KikWybor() umozliwiajacy wyswietlenie planszy wyboru kto rozpoczyna gre oraz jaki znak rozpoczyna.
     */
    public static KikWybor zapytanie=new KikWybor();
    /**
     *  Statyczny JFrame aplikacji Kolko krzyzyk
     */
    public static JFrame frameApp = new JFrame();
    /**
     *  Zmienna logiczna przechowujaca informacje o tym, czy nastapil juz Remis
     */
    private boolean czyRemis=false;


    /**
     *  Funkcja jest odpowiedzialna za wyswietlenie glownej planszy gry Kolko Krzyzyk
     */
    public void pokazPlansze(){

            frameApp.setContentPane(new App().panelName);
            frameApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frameApp.pack();
            frameApp.setVisible(true);
            frameApp.setLocationRelativeTo(null);
            //zapytanie.frame.setVisible(false);
            zapytanie.frame.dispose();
            frameApp.setResizable(false);
            frameApp.setMinimumSize(new Dimension(700,680));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
        Point newLocation = new Point(middle.x - (frameApp.getWidth() / 2),
                middle.y - (frameApp.getHeight() / 2));
        frameApp.setLocation(newLocation);

    }


    /**
     *  Konstruktor umozliwiajacy przypisanie danych startowych atrybutow gry Kolko Krzyzyk
     */
    public App() {

        for(int i=0; i<10; i++)
        {
            tab[i]="n";
        }
        tablicaprzyciskow[1]=button1;
        tablicaprzyciskow[2]=button2;
        tablicaprzyciskow[3]=button3;
        tablicaprzyciskow[4]=button4;
        tablicaprzyciskow[5]=button5;
        tablicaprzyciskow[6]=button6;
        tablicaprzyciskow[7]=button7;
        tablicaprzyciskow[8]=button8;
        tablicaprzyciskow[9]=button9;
        przypiszDaneWejsciowe();
        for (int i=1;i<10;i++){
            tablicaprzyciskow[i].setBorderPainted(false);
        }


        buttonReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                iloscruchow=0;
                for (int i=1;i<10;i++){
                    czyklikniety[i]=0;
                    tab[i]="n";
                    tablicaprzyciskow[i].setIcon(new ImageIcon(Class.class.getResource("/puste.png")));
                    tablicaprzyciskow[i].setEnabled(true);
                }
                //zapytanie.frame.setVisible(true);
                //zapytanie.frame.repaint();

                zapytanie.pokazOknoReset();
                //frameApp.dispose();
            }
        });

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //JOptionPane.showMessageDialog(null, "hello");
                if(czyklikniety[1]==0){
                    pole1wywolanie();
                    if(czygrazkomputerem==true){
                        ruchkomputera();
                    }
                }
                else
                {
                    JOptionPane.showConfirmDialog(null,"Ups... Pole jest już zajęte! Wybierz inne","Mamy problem...", JOptionPane.CLOSED_OPTION);
                }
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(czyklikniety[2]==0){
                    pole2wywolanie();
                    if(czygrazkomputerem==true){
                        ruchkomputera();
                    }
                }
                else
                {
                    JOptionPane.showConfirmDialog(null,"Ups... Pole jest już zajęte! Wybierz inne","Mamy problem...", JOptionPane.CLOSED_OPTION);
                }
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(czyklikniety[3]==0){
                    pole3wywolanie();
                    if(czygrazkomputerem==true){
                        ruchkomputera();
                    }
                }
                else
                {
                    JOptionPane.showConfirmDialog(null,"Ups... Pole jest już zajęte! Wybierz inne","Mamy problem...", JOptionPane.CLOSED_OPTION);
                }
            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(czyklikniety[4]==0){
                    pole4wywolanie();
                    if(czygrazkomputerem==true){
                        ruchkomputera();
                    }
                }
                else
                {
                    JOptionPane.showConfirmDialog(null,"Ups... Pole jest już zajęte! Wybierz inne","Mamy problem...", JOptionPane.CLOSED_OPTION);
                }
            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(czyklikniety[5]==0){
                    pole5wywolanie();
                    if(czygrazkomputerem==true){
                        ruchkomputera();
                    }
                }
                else
                {
                    JOptionPane.showConfirmDialog(null,"Ups... Pole jest już zajęte! Wybierz inne","Mamy problem...", JOptionPane.CLOSED_OPTION);
                }
            }
        });
        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(czyklikniety[6]==0){
                    pole6wywolanie();
                    if(czygrazkomputerem==true){
                        ruchkomputera();
                    }
                }
                else
                {
                    JOptionPane.showConfirmDialog(null,"Ups... Pole jest już zajęte! Wybierz inne","Mamy problem...", JOptionPane.CLOSED_OPTION);
                }
            }
        });
        button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(czyklikniety[7]==0){
                    pole7wywolanie();
                    if(czygrazkomputerem==true){
                        ruchkomputera();
                    }
                }
                else
                {
                    JOptionPane.showConfirmDialog(null,"Ups... Pole jest już zajęte! Wybierz inne","Mamy problem...", JOptionPane.CLOSED_OPTION);
                }
            }
        });
        button8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(czyklikniety[8]==0){
                    pole8wywolanie();
                    if(czygrazkomputerem==true){
                        ruchkomputera();
                    }
                }
                else
                {
                    JOptionPane.showConfirmDialog(null,"Ups... Pole jest już zajęte! Wybierz inne","Mamy problem...", JOptionPane.CLOSED_OPTION);
                }
            }
        });
        button9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(czyklikniety[9]==0){
                    pole9wywolanie();
                    if(czygrazkomputerem==true){
                        ruchkomputera();
                    }
                }
                else
                {
                    JOptionPane.showConfirmDialog(null,"Ups... Pole jest już zajęte! Wybierz inne","Mamy problem...", JOptionPane.CLOSED_OPTION);
                }
            }
        });


        buttonBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Start powrot = new Start();
                powrot.pokazPlansze();
            }
        });
    }
    /**
     *  Funkcja jest odpowiedzialna za pobranie z formularza KikWybor danych kto rozpoczyna gre oraz jaki znak rozpoczyna
     */
    public void przypiszDaneWejsciowe(){

        ktogra=zapytanie.znak_gracza;
        changeImageSmall(buttonTura);

        if(zapytanie.jaka_gra=="pvp")czygrazkomputerem=false;
        else {
            turaNapis.setText("Twój znak: ");
            czygrazkomputerem=true;
        }

        if(zapytanie.kto_zaczyna=="k"){
            turaNapis.setText("Twój znak: ");
            ruchkomputera();
        }
    }
    //wywolanie akcji dla konkretnych pol
    /**
     *  Funkcja jest odpowiedzialna za obsluge wyboru pola numer 1
     */
    private void pole1wywolanie(){
        iloscruchow++;
        tab[1]=ktogra;

        changeImage(button1);
        if(czyWygrana==false && czyRemis==false)changeImageSmall(buttonTura);
        czyklikniety[1]=1;

    }
    /**
     *  Funkcja jest odpowiedzialna za obsluge wyboru pola numer 2
     */
    private void pole2wywolanie(){
        iloscruchow++;
        tab[2]=ktogra;

        changeImage(button2);
        if(czyWygrana==false && czyRemis==false)changeImageSmall(buttonTura);
        czyklikniety[2]=1;
    }
    /**
     *  Funkcja jest odpowiedzialna za obsluge wyboru pola numer 3
     */
    private void pole3wywolanie(){
        iloscruchow++;
        tab[3]=ktogra;

        changeImage(button3);
        if(czyWygrana==false && czyRemis==false)changeImageSmall(buttonTura);
        czyklikniety[3]=1;
    }
    /**
     *  Funkcja jest odpowiedzialna za obsluge wyboru pola numer 4
     */
    private void pole4wywolanie(){
        iloscruchow++;
        tab[4]=ktogra;

        changeImage(button4);
        if(czyWygrana==false && czyRemis==false)changeImageSmall(buttonTura);
        czyklikniety[4]=1;
    }
    /**
     *  Funkcja jest odpowiedzialna za obsluge wyboru pola numer 5
     */
    private void pole5wywolanie(){
        iloscruchow++;
        tab[5]=ktogra;

        changeImage(button5);
        if(czyWygrana==false && czyRemis==false)changeImageSmall(buttonTura);
        czyklikniety[5]=1;
    }
    /**
     *  Funkcja jest odpowiedzialna za obsluge wyboru pola numer 6
     */
    private void pole6wywolanie(){
        iloscruchow++;
        tab[6]=ktogra;

        changeImage(button6);
        if(czyWygrana==false && czyRemis==false)changeImageSmall(buttonTura);
        czyklikniety[6]=1;
    }
    /**
     *  Funkcja jest odpowiedzialna za obsluge wyboru pola numer 7
     */
    private void pole7wywolanie(){
        iloscruchow++;
        tab[7]=ktogra;

        changeImage(button7);
        if(czyWygrana==false && czyRemis==false)changeImageSmall(buttonTura);
        czyklikniety[7]=1;
    }
    /**
     *  Funkcja jest odpowiedzialna za obsluge wyboru pola numer 8
     */
    private void pole8wywolanie(){
        iloscruchow++;
        tab[8]=ktogra;

        changeImage(button8);
        if(czyWygrana==false && czyRemis==false)changeImageSmall(buttonTura);
        czyklikniety[8]=1;
    }
    /**
     *  Funkcja jest odpowiedzialna za obsluge wyboru pola numer 9
     */
    private void pole9wywolanie(){
        iloscruchow++;
        tab[9]=ktogra;

        changeImage(button9);
        if(czyWygrana==false && czyRemis==false)changeImageSmall(buttonTura);
        czyklikniety[9]=1;
    }
    /**
     *  Funkcja jest odpowiedzialna za zmiane ikony przycisku wyswietlajacego znak aktualnie rozgrywajacego gracza
     */
    public void changeImageSmall(JButton button){
        if (ktogra == "o") {
            button.setIcon(new ImageIcon(Class.class.getResource("/kolkos.png")));
        } else {
            button.setIcon(new ImageIcon(Class.class.getResource("/krzyzyks.png")));
        }
    }
    /**
     *  Funkcja jest odpowiedzialna za zmiane ikony przycisku danego pola wybranego przez gracza/komputer oraz za sprawdzenie czy dane pole jest juz zajete
     */
    public void changeImage(JButton button) {

        if (ktogra == "o") {
            button.setIcon(new ImageIcon(Class.class.getResource("/kolko.png")));
            ktogra = "x";

        } else {
            button.setIcon(new ImageIcon(Class.class.getResource("/krzyzyk.png")));
            ktogra = "o";

        }


        if (sprawdz()) {
            for (int i = 1; i < 10; i++) {
                if (tab[i] == "n") {
                    if (tab[i] == "n") {
                        tablicaprzyciskow[i].setEnabled(false);
                    }
                }
            }
            if (ktogra == "x") ktogra = "o";
            else ktogra = "x";
            changeImageSmall(buttonTura);
            if (ktogra == "x") ktogra = "X";
            else ktogra = "O";
            turaNapis.setText("Wygrany: ");
            JOptionPane.showConfirmDialog(null, "Wygrywa gracz ze znakiem: " + ktogra, "Mamy zwycięzce!!!", JOptionPane.CLOSED_OPTION);
            czyWygrana=true;
        }
    }
    /**
     *  Funkcja odpowiedzialna za wywolanie funkcji odpowiedzialnej za pokazanie okna za wyborem opcji gry
     */
    public static void startKik(){
        zapytanie.pokazOkno();
    }
    /**
     *  Funkcja jest odpowiedzialna za sprawdzenie czy nastapila wygrana lub remis.
     */
    public boolean sprawdz () {
        if
        (
                (tab[1] == tab[2] && tab[2] == tab[3] && tab[1] != "n") ||
                        (tab[4] == tab[5] && tab[5] == tab[6] && tab[4] != "n") ||
                        (tab[7] == tab[8] && tab[8] == tab[9] && tab[7] != "n") ||
                        (tab[1] == tab[4] && tab[4] == tab[7] && tab[1] != "n") ||
                        (tab[2] == tab[5] && tab[5] == tab[8] && tab[2] != "n") ||
                        (tab[3] == tab[6] && tab[6] == tab[9] && tab[3] != "n") ||
                        (tab[1] == tab[5] && tab[5] == tab[9] && tab[1] != "n") ||
                        (tab[3] == tab[5] && tab[5] == tab[7] && tab[3] != "n")
        ) {
            return true;
        }

        else if (iloscruchow == 9) {
            JOptionPane.showConfirmDialog(null, "Remis!!!", "Mamy remis :-)", JOptionPane.CLOSED_OPTION);
            czygrazkomputerem=false;
            czyRemis=true;
            return false;
        } else return false;
    }
    /**
     *  Funkcja jest odpowiedzialna za sprawdzenie, ktory przycisk zostal klikniety
     */
    private void wyborw ( int w)
    {
        if (w == 1) {
            pole1wywolanie();
        } else if (w == 2) {
            pole2wywolanie();
        } else if (w == 3) {
            pole3wywolanie();
        } else if (w == 4) {
            pole4wywolanie();
        } else if (w == 5) {
            pole5wywolanie();
        } else if (w == 6) {
            pole6wywolanie();
        } else if (w == 7) {
            pole7wywolanie();
        } else if (w == 8) {
            pole8wywolanie();
        } else if (w == 9) {
            pole9wywolanie();
        }
    }
    /**
     *  Funkcja jest odpowiedzialna za ruch komputera
     */
    private void ruchkomputera()
    {
        int w = 0;
        String ktograzmiana;
        ktograzmiana = ktogra;
        if (ktograzmiana == "x") ktograzmiana = "o";
        else ktograzmiana = "x";

        for (int i = 1; i < 10; i++) {
            if (tab[i] == "n") {
                tab[i] = ktogra;
                boolean test = sprawdz();
                if (test == true) {
                    w = i;
                    tab[i] = "n";
                    break;
                }
                tab[i] = "n";
            }
        }
        if (w == 0) {
            for (int i = 1; i < 10; i++) {
                if (tab[i] == "n") {
                    tab[i] = ktograzmiana;
                    boolean test = sprawdz();
                    if (test == true) {
                        w = i;
                    }
                    tab[i] = "n";
                }
            }
        }

        if (w > 0) {
            wyborw(w);
        } else {
            //liczba pseudolosowa

            Random generator = new Random();
            int pierwszyRuchKomp=0;
            pierwszyRuchKomp=generator.nextInt(8)+1;

            if (tab[1] == tab[2] && tab[2] == tab[3] && tab[3] == tab[4]
                    && tab[4] == tab[5] && tab[5] == tab[6] && tab[6] == tab[7] && tab[7] == tab[8] && tab[8] == tab[9] && tab[5] == "n") {
                if (tab[1] == tab[2] && tab[3] == "n") {
                    if(pierwszyRuchKomp==1)pole1wywolanie();
                    else if(pierwszyRuchKomp==2)pole2wywolanie();
                    else if(pierwszyRuchKomp==3)pole3wywolanie();
                    else if(pierwszyRuchKomp==4)pole4wywolanie();
                    else if(pierwszyRuchKomp==5)pole5wywolanie();
                    else if(pierwszyRuchKomp==6)pole6wywolanie();
                    else if(pierwszyRuchKomp==7)pole7wywolanie();
                    else if(pierwszyRuchKomp==8)pole8wywolanie();
                    else pole9wywolanie();
                }
            } else {
                if (tab[5] == "n") {
                    pole5wywolanie();
                } else if ((tab[3] == tab[7]) && tab[7] != "n" && tab[8] == "n") {
                    pole8wywolanie();
                } else if ((tab[1] == tab[9]) && tab[9] != "n" && tab[8] == "n") {
                    pole8wywolanie();
                } else if ((tab[4] == tab[3]) && tab[3] != "n" && tab[1] == "n") {
                    pole1wywolanie();
                } else if ((tab[2] == tab[7]) && tab[7] != "n" && tab[1] == "n") {
                    pole1wywolanie();
                } else if ((tab[2] == tab[9]) && tab[9] != "n" && tab[3] == "n") {
                    pole3wywolanie();
                } else if ((tab[4] == tab[9]) && tab[9] != "n" && tab[7] == "n") {
                    pole7wywolanie();
                } else if (tab[2] == tab[6] && tab[2] != "n" && tab[3] == "n") pole3wywolanie();
                else if (tab[2] == tab[6] && tab[2] != "n" && tab[9] == "n") pole9wywolanie();
                else if (tab[2] == tab[4] && tab[2] != "n" && tab[3] == "n") pole3wywolanie();
                else if (tab[4] == tab[8] && tab[4] != "n" && tab[7] == "n") pole7wywolanie();
                else if (tab[4] == tab[8] && tab[4] != "n" && tab[9] == "n") pole9wywolanie();
                else if ((tab[3] == "x" || tab[3] == "o") && tab[1] == tab[2] && tab[2] == tab[8] && tab[8] == tab[4]
                        && tab[4] == tab[5] && tab[5] == tab[6] && tab[6] == tab[7] && tab[7] == tab[9] && tab[5] == "n")
                    pole5wywolanie();
                else if ((tab[7] == "x" || tab[7] == "o") && tab[1] == tab[2] && tab[2] == tab[3] && tab[3] == tab[4]
                        && tab[4] == tab[5] && tab[5] == tab[6] && tab[6] == tab[8] && tab[8] == tab[9] && tab[5] == "n")
                    pole5wywolanie();
                else if ((tab[8] == "x" || tab[8] == "o") && tab[1] == tab[2] && tab[2] == tab[3] && tab[3] == tab[4]
                        && tab[4] == tab[5] && tab[5] == tab[6] && tab[6] == tab[7] && tab[7] == tab[9] && tab[5] == "n")
                    pole5wywolanie();
                else if ((tab[6] == "x" || tab[6] == "o") && tab[1] == tab[2] && tab[2] == tab[3] && tab[3] == tab[4]
                        && tab[4] == tab[5] && tab[5] == tab[8] && tab[8] == tab[7] && tab[7] == tab[9] && tab[5] == "n")
                    pole5wywolanie();
                else if ((tab[9] == "x" || tab[9] == "o") && tab[1] == tab[2] && tab[2] == tab[3] && tab[3] == tab[4]
                        && tab[4] == tab[5] && tab[5] == tab[6] && tab[6] == tab[7] && tab[7] == tab[8] && tab[5] == "n")
                    pole5wywolanie();
                else if (tab[1] == tab[2] && tab[3] == "n") pole3wywolanie();
                else if (tab[1] == tab[4] && tab[7] == "n") pole7wywolanie();
                else if (tab[1] == tab[5] && tab[9] == "n") pole9wywolanie();
                else if (tab[5] == tab[9] && tab[1] == "n") pole1wywolanie();
                else if (tab[3] == tab[2] && tab[1] == "n") pole1wywolanie();
                else if (tab[3] == tab[1] && tab[2] == "n") pole2wywolanie();
                else if (tab[2] == tab[8] && tab[5] == "n") pole5wywolanie();
                else if (tab[2] == tab[5] && tab[8] == "n") pole8wywolanie();
                else if (tab[8] == tab[5] && tab[2] == "n") pole2wywolanie();
                else if (tab[3] == tab[6] && tab[9] == "n") pole9wywolanie();
                else if (tab[6] == tab[9] && tab[3] == "n") pole3wywolanie();
                else if (tab[3] == tab[9] && tab[6] == "n") pole6wywolanie();
                else if (tab[7] == tab[5] && tab[3] == "n") pole3wywolanie();
                else if (tab[3] == tab[5] && tab[7] == "n") pole7wywolanie();
                else if (tab[3] == tab[7] && tab[5] == "n") pole5wywolanie();
                else if (tab[1] == tab[7] && tab[4] == "n") pole4wywolanie();
                else if (tab[4] == tab[7] && tab[1] == "n") pole1wywolanie();
                else if (tab[4] == tab[7] && tab[5] == "n") pole6wywolanie();
                else if (tab[4] == tab[6] && tab[5] == "n") pole5wywolanie();
                else if (tab[5] == tab[6] && tab[4] == "n") pole4wywolanie();
                else if (tab[7] == tab[8] && tab[9] == "n") pole9wywolanie();
                else if (tab[7] == tab[9] && tab[8] == "n") pole8wywolanie();
                else if (tab[8] == tab[9] && tab[7] == "n") pole7wywolanie();
                else if (tab[1] == tab[9] && tab[5] == "n") pole5wywolanie();
                else {
                    for (int i = 1; i <= 10; i++) {
                        if (tab[i] == "n") {
                            w = i;
                            break;
                        }
                    }
                    if (w != 0) {
                        wyborw(w);
                    }
                }

            }
        }
    }
}

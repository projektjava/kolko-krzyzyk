package KiK;


import org.apache.poi.ss.usermodel.*;
import java.io.File;
import java.io.IOException;

public class DataBase {
    /**
     *  Zmienna przechowująca scciezke do pliku w formaice xls/xlsx
     */
       // public static final String SAMPLE_XLSX_FILE_PATH = "Baza.xls";
    /**
     *  Zmienna poszczególne przechowujaca rekordy bazy danych
     */
        private String[][] base ;
    /**
     *  Zmienne iteratorow wiersza i kolumny
     */
        private   int rowIterator =0, cellIterator =0;
    /**
     *  Konstruktor bezargumentowy tworzacy nowe obiekty bazy
     */
         DataBase() throws IOException {

            // Workbook workbook = WorkbookFactory.create(new File("C:\\Users\\Arek\\Desktop\\java\\projektjava\\Baza.xls"));
             Workbook workbook = WorkbookFactory.create(new File("Baza.xls"));
             Sheet sheet = workbook.getSheet("Arkusz1");
             int number = sheet.getLastRowNum();
             number++;
             System.out.println("number  =  " + number);
             base = new String[number][number];
             for (Row row : sheet) {
                 for (Cell cell : row) {

                     String cellValue = String.valueOf(cell);
                     System.out.print(cellValue + " |");
                     if (cell != null) {
                         System.out.println("row = " + rowIterator + "cell = " + cellIterator);
                         base[rowIterator][cellIterator] = cellValue;
                     }

                     cellIterator++;
                 }
                 System.out.println();
                 cellIterator = 0;
                 rowIterator++;
             }

             // Closing the workbook
             workbook.close();
         }
    /**
     *  Metoda zwracajaca rowIterator (getter)
     * @return rowIterator - liczba wierszy bazy
     */
        public int getRowIterator(){
             return rowIterator;
        }
    /**
     *  Metoda zwracajaca tablice dwuwymiarowa odpowiadajaca pobranej bazie danych , gdzie pierwsza kolumna to kategoria , a droga to haslo
     * @return base - tablica odpowiadajaca bazie danych
     */
        public String[][] getBase(){
             return base;
        }
}


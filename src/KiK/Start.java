package KiK;

import Battleships.BattleBoards;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 *   @author Kacper Seweryn
 *   @version 1.0
 **/

public class Start {
    /**
     *  Przycisk odpowiedzialny za uruchomienie gry Kolko i Krzyzyk
     */
    private JButton kikStart;
    /**
     *  Przycisk odpowiedzialny za uruchomienie gry Statki
     */
    private JButton battleshipsStart;
    /**
     *  Glowny panel w ktorym znajduja sie wszystkie elementy typu JButton czy JTextArea
     */
    private JPanel panelName;
    /**
     *  Przycisk odpowiedzialny za uruchomienie gry Wisielec
     */
    private JButton wisielecStart;
    /**
     *  Utworzenie statycznego obiektu gry KiK
     */
    public static App KikGra=new App();
    /**
     *  Utworzenie statycznej (aby latwiej bylo nia zarzadzac) ramki Start
     */
    public static JFrame frameStart = new JFrame();
    /**
     *  Utworzenie statycznej (aby latwiej bylo nia zarzadzac) ramki Pola wyboru opcji gry Wisielca
     */
    public static JFrame frameWisielecWybor=new JFrame();
/**
     *  Utworzenie statycznej (aby latwiej bylo nia zarzadzac) ramki gry Statki
     */
    public static JFrame frameStatki = new JFrame();

    /**
     *  Utworzenie statycznej (aby latwiej bylo nia zarzadzac) ramki penelu Winner
     */
    public static JFrame frameWygrany = new JFrame();
    /**
     *  Utworzenie statycznej (aby latwiej bylo nia zarzadzac) ramki gry Wisielec
     */
    public static JFrame frameWisielecGra = new JFrame();

    /**
     *  Konstruktor klasy Start odpowiedzialny za przypisanie ActionListenerow do odpowiednych buttonow
     */
    public Start() {
        kikStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                KikGra.startKik();
                frameStart.setVisible(false);
            }
        });
        battleshipsStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            pokazPlanszeStatki();
            }
        });
        wisielecStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PokazWisielecWybor();
            }
        });
    }
    /**
     *  Funkcja odpowiedzialna za wyswietlenie planszy z wyborem opcji gry WISIELEC
     */
    public static void PokazWisielecWybor(){
        frameWisielecWybor.setContentPane(new WisielecWybor().panelName);
        frameWisielecWybor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frameWisielecWybor.pack();
        frameWisielecWybor.setVisible(true);
        frameWisielecWybor.setLocationRelativeTo(null);
        frameWisielecWybor.setMinimumSize(new Dimension(700,600));
        frameWisielecWybor.setPreferredSize(new Dimension(700,600));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
        Point newLocation = new Point(middle.x - (frameWisielecWybor.getWidth() / 2),
                middle.y - (frameWisielecWybor.getHeight() / 2));
        frameWisielecWybor.setLocation(newLocation);
        frameStart.setVisible(false);


    }
    /**
     *  funkcja odpowiedzialna za ukrycie planszy gry KiK oraz uwidocznienie planszy Start, wykorzystywana przy powrocie do Menu z gry KiK.
     */
    public void pokazPlansze(){
        frameStart.setVisible(true);
        App.frameApp.dispose();
    }
    /**
     *  Konstruktor przypisujacy dane oraz parametry startowe poszczegolnych obiektow gry statki takie jak np. wielkosc JFrame, widocznosc i wiele innych.
     */
    public static void pokazPlanszeStatki() {

        frameStatki.setContentPane(new BattleBoards().getMainPanel());
        frameStatki.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frameStatki.setExtendedState(JFrame.MAXIMIZED_BOTH); //FULLSCREEN
        ImageIcon ico = new ImageIcon("C:\\Users\\Arek\\Desktop\\java\\projektjava\\src\\Battleships\\image\\water.png");
        frameStatki.setIconImage(ico.getImage());
        frameStatki.pack();
        frameStatki.setVisible(true);
        frameStart.setVisible(false);

    }

    /**
     *  funkcja main uruchamiajaca sie jako pierwsza, nadaje parametry dla panelu Menu oraz ustawia go na srodku ekranu.
     */
    public static void main (String[]args){
        frameStart.setContentPane(new Start().panelName);
        frameStart.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frameStart.pack();
        frameStart.setVisible(true);
        frameStart.setLocationRelativeTo(null);
        frameStart.setMinimumSize(new Dimension(700,600));
        frameStart.setPreferredSize(new Dimension(700,600));

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
        Point newLocation = new Point(middle.x - (frameStart.getWidth() / 2),
                middle.y - (frameStart.getHeight() / 2));
        frameStart.setLocation(newLocation);
    }
}

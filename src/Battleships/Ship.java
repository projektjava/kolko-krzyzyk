package Battleships;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ship {
    /**
     *Zmienna przechowujaca liczbe masztow statku
     */
    private int numberOfMasts;
    /**
     *Zmienna buttonu statku (graficzna reprezentacja staku na planszy)
     */
    private JButton button;
    /**
     *Zmienna przechowujaca aktualny statek( zapisywanie this przed Action listenerem)
     */
    private Ship ship;
    /**
     *Zmienna stanu czy statek został ustawiony;
     */
    private boolean isSet=false;
    /**
     *Konstruktor staku tworzacy nowy staek w zaleznosci od liczby masztow oraz kontekstu
     * @param masts - liczba masztow aktualnie tworzonego statku
     * @param Board - kontekst (referencja do obiektu BattleBoards , który wywołuje tworzenie statku)
     */
    Ship(int masts , final BattleBoards Board) {
        int boardSize = Board.getBoardSize();
        numberOfMasts = masts;
       button = new JButton();
        button.setName(String.valueOf(masts));
        button.setPreferredSize(new Dimension((masts * boardSize), boardSize));
        button.setMaximumSize(new Dimension((masts * boardSize), boardSize));
        button.setMinimumSize(new Dimension((masts * boardSize), boardSize));
        button.setSize(new Dimension((masts * boardSize), boardSize));
        ImageIcon ico = null;
        switch (masts){
            case 1:
                ico = new ImageIcon(Class.class.getResource("/Battleships/image/oneMast.png"));
                break;
            case 2:
                ico = new ImageIcon(Class.class.getResource("/Battleships/image/Masts2.png"));
                break;
            case 3:
                ico = new ImageIcon(Class.class.getResource("/Battleships/image/Masts3.png"));
                break;
            case 4:
                ico = new ImageIcon(Class.class.getResource("/Battleships/image/Masts4.png"));
                break;
                default:
                    System.out.println("Nieokreslony blad  liczba masztów="+masts);
        }

        button.setIcon(ico);
        button.setBackground(Color.RED);
        ship=this;
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              //  button.setEnabled(false);
                Board.setSelectedShip(ship);

            }
        });

    }

    /**
     *Metoda zwracajaca button (reprezentacje statku w GUI)(getter)
     * @return button - button reprezetujacy statek
     */
    public JButton getButton() {
        return button;
    }
    /**
     *Metoda zwracajaca liczbe masztow statku(getter)
     * @return numberOfMasts - liczba masztów statku
     */
    public int getNumberOfMasts() {
        return numberOfMasts;
    }
    /**
     *Metoda ustawiajaca stan statku (czy został juz ustawiony na planszy ) (setter)
     * @param state - zmienna logiczna (true jesli został ustawiony na planszy , w przeciwnym wypadku false)
     */
    public void setIsSet(boolean state){
        isSet=state;
    }
    /**
     *Metoda zwracajaca stan statku (czy został juz ustawiony na planszy );
     * @return isSet - zmenna logiczn (true jesli zostal ustawiony , w przeciwnym wypadku flase);
     */
    public boolean  getIsSet(){
        return isSet;
    }
}

/*
 * Created by JFormDesigner on Fri Nov 09 20:48:28 CET 2018
 */

package Battleships;

import KiK.Start;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Arkadiusz Olek
 */
public class Winner2 extends JFrame {
    /**
     * Zmienna prywatna zawierająca ramkę paenelu wygranego ;
     */
    private JFrame winner;
    /**
     * Konstruktor zmieniajacy tlo w zaleznosci od tego kto wygral (gracz czy komputer)
     * @param player - zmienna loginczna (true jezeli wygral gracz , falese jesli przegral)
     */
    public Winner2(boolean player) {
        initComponents();
        ImageIcon ico;
        if(player){
            ico = new ImageIcon(Class.class.getResource("/Battleships/image/win.png"));
        }
        else{
            ico = new ImageIcon(Class.class.getResource("/Battleships/image/Defeat.png"));
        }
        label1.setIcon(ico);
    winner=this;
    }

    /**
     *
     * Metoda obslugujaca klikniecie w przycisk powrotu do menu glownego , tworzy ona nowy obiekt Start() i zamyka frame ze statami oraz wygraną
     * @param e - Action event (default)
     */
    private void btReturnActionPerformed(ActionEvent e) {
        Start powrot = new Start();
        powrot.pokazPlansze();
        Start.frameStatki.dispose();
        Start.frameWygrany.dispose();

    }
/**
 *
 * Metoda obslugujaca klikniecie w przycisku grania jeszcze raz , uruchamia ona nowa gre w statki oraz zamyka poprzednie okienka
 * @param e - Action event (default)
 */
    private void btPlayAginActionPerformed(ActionEvent e) {
        Start.frameStatki.dispose();
        Start.pokazPlanszeStatki();
        Start.frameWygrany.dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Arek
        mainPanel = new JPanel();
        btReturn = new JButton();
        btPlayAgin = new JButton();
        label1 = new JLabel();

        //======== this ========
        setIconImage(new ImageIcon(getClass().getResource("/water.png")).getImage());
        setAlwaysOnTop(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== mainPanel ========
        {

            // JFormDesigner evaluation mark
            mainPanel.setBorder(new javax.swing.border.CompoundBorder(
                new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                    "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
                    javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                    java.awt.Color.red), mainPanel.getBorder())); mainPanel.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

            mainPanel.setLayout(null);

            //---- btReturn ----
            btReturn.setText("Powr\u00f3t do menu");
            btReturn.setIcon(new ImageIcon(getClass().getResource("/Battleships/image/return.png")));
            btReturn.setPreferredSize(new Dimension(130, 60));
            btReturn.setHorizontalAlignment(SwingConstants.LEFT);
            btReturn.setBorder(null);
            btReturn.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
            btReturn.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    btReturnActionPerformed(e);
                }
            });
            mainPanel.add(btReturn);
            btReturn.setBounds(75, 555, 130, 60);

            //---- btPlayAgin ----
            btPlayAgin.setText("Graj jeszcze raz");
            btPlayAgin.setIcon(new ImageIcon(getClass().getResource("/playAgin.png")));
            btPlayAgin.setBorder(null);
            btPlayAgin.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
            btPlayAgin.setPreferredSize(new Dimension(130, 60));
            btPlayAgin.setHorizontalAlignment(SwingConstants.LEFT);
            btPlayAgin.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    btPlayAginActionPerformed(e);
                }
            });
            mainPanel.add(btPlayAgin);
            btPlayAgin.setBounds(405, 555, 130, 60);

            //---- label1 ----
            label1.setText("text");
            label1.setIcon(new ImageIcon(getClass().getResource("/win.png")));
            label1.setPreferredSize(new Dimension(600, 660));
            mainPanel.add(label1);
            label1.setBounds(5, 0, 610, 660);

            { // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < mainPanel.getComponentCount(); i++) {
                    Rectangle bounds = mainPanel.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = mainPanel.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                mainPanel.setMinimumSize(preferredSize);
                mainPanel.setPreferredSize(preferredSize);
            }
        }
        contentPane.add(mainPanel);
        mainPanel.setBounds(0, 0, 600, 660);

        { // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    /**
     *
     * Metoda zwracajaca panel glowny (mainPanel) (getter)
     * @return mainPanel - glowny panel obejmojacy cale okienko potrzebny do stworzenia nowego okienka w kodzie
     * */
    public JPanel getPanel(){
        return  mainPanel;
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Arek
    private JPanel mainPanel;
    private JButton btReturn;
    private JButton btPlayAgin;
    private JLabel label1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}

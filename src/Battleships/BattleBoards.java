package Battleships;


import KiK.Start;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author Arkadiusz Olek
 */
public class BattleBoards {
    /**
     *  Panel zawierajacy plansze gracza
     */
    private JPanel myBoard;
    /**
     *  Panel zawierajacy plansze przeciwnika
     */
    private JPanel enemyBoard;
    /**
     *  Panel glowny zawierajacy pozostałe panele
     */
    private JPanel mainPanel;
    /**
     *  Panel zawierajacy statki mozliwe do ustawienia
     */
    private JPanel myGameInfoPanel;
    /**
     *  Button powrotu do menu
     */
    private JButton buttonBack;
    /**
     *  Zmienna zawierajaca wielkosc pojedynczej komorki na planszy
     */
    private int boardSize=60;
    /**
     *  Zmienna zawirajaca liczbę masztow wybranego statku
     */
    private int numberOfMastsOfSelectedShip;
    /**
     *  Zmienna zawierajaca wybrany statek
     */
    private Ship selectedShip=null;
    /**
     *  Tablica zawierajaca wszystkie buttony planszy gracza
     */
    private JButton[][] myButtonTable = new JButton[10][10];
    /**
     *  Tablica zawierajaca wszystkie buttony planszy przeciwnika
     */
    private JButton[][] enemyButtonTable = new JButton[10][10];
    /**
     *  Zmienna zawierajaca ostatio klikniety button na planszy;
     */
    private String lastBoardClickedButton;
    /**
     *  Tablica na ktorej rozgrywa sie gra  (morze gracza)
     */
    private int[][] myBattleBoard = new int[10][10];
    /**
     *  Tablica na ktorej rozgrywa sie gra  (morze przeciwnika)
     */
    private int[][] enemyBattleBoard = new int [10][10];

    //Zmienne potrzebne do działania bota
    /**
     *  Zmienna stanu trafienia przez bota (jesli bot w ostatniej turze trafil w statek zmienna przyjmuje wartosc true)
     */
    private boolean lastBotShoot = false;
    /**
     *  Zmienna stanu strony w którą bot zaczyna szukać statku po pojedynczym trafieniu
     */
    private int side = 0;
    /**
     *  Zmienna przechowująca wygenerowany losowo w poprzedniej lub poprzednich turach wiersz tablicy (tylko jesli bot trafil w statek)
     */
    private int firstBotCord=0;
    /**
     *  Zmienna przechowująca wygenerowana losowo w poprzedniej lub poprzednich turach kolumne tablicy (tylko jesli bot trafił w statek)
     */
    private int secondBotCord=0;
    /**
     *  Zmienna przechowujaca liczbe wywolan funkcji theTargetWasHit()
     */
    private int call = 0;
    /**
     *  Zmienna stanu czy statek został zatopiony przez bota podczas pierwszego szukania (w pionie)
     */
    private boolean sank =false;

    /**
     *  Konstruktor bezargumoenwy ładujący planszę i rozpoczynający grę
     */
    public BattleBoards(){

        ImageIcon ico =  new ImageIcon(Class.class.getResource("/Battleships/image/water2.png"));

        buttonBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Start powrot = new Start();
                powrot.pokazPlansze();
                Start.frameStatki.dispose();
            }
        });


        Container myBoardPanel;
        myBoardPanel = myBoard;
        myBoardPanel.setLayout(new GridLayout(10, 10));
        for(int i = 0; i < 10; i++) {
            for(int k =0 ; k<10;k++) {

                final JButton button = new JButton(ico);
               button.setName(String.valueOf(i)+String.valueOf(k));

                myButtonTable[i][k]=button;
                button.setPreferredSize(new Dimension(boardSize, boardSize));
                button.setMaximumSize(new Dimension(boardSize, boardSize));
                button.setMinimumSize(new Dimension(boardSize, boardSize));
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        myButtonClick(button);
                    }
                });
                myBoardPanel.add(button);
                myBattleBoard[i][k]=0;
            }
        }

        Container enemyBoardPanel;
        enemyBoardPanel = enemyBoard;
        enemyBoardPanel.setLayout(new GridLayout(10, 10));
        for(int j = 0; j < 10; j++) {
            for(int k =0 ; k<10;k++) {

                final JButton button = new JButton(ico);
                button.setDisabledIcon(ico);
                enemyButtonTable[j][k]=button;
                button.setName(String.valueOf(j)+String.valueOf(k));
                button.setPreferredSize(new Dimension(boardSize, boardSize));
                button.setMaximumSize(new Dimension(boardSize, boardSize));
                button.setMinimumSize(new Dimension(boardSize, boardSize));
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        enemyButtonClick(button);
                    }
                });
                enemyBattleBoard[j][k] = 0;
                enemyBoardPanel.add(button);
            }
        }
        Container myGameInfoPanelContainer;
        myGameInfoPanelContainer=myGameInfoPanel;
        myGameInfoPanelContainer.setLayout(new GridBagLayout());






            Ship battleship1 = new Ship(4,this);
            myGameInfoPanelContainer.add(battleship1.getButton());
            for (int i=0;i<2;i++){
                Ship battleship2 = new Ship(3,this);
                myGameInfoPanelContainer.add(battleship2.getButton());
        }

        for (int i=0;i<3;i++){
            Ship battleship2 = new Ship(2,this);
            myGameInfoPanelContainer.add(battleship2.getButton());
        }
        for (int i=0;i<4;i++){
            Ship battleship2 = new Ship(1,this);
            myGameInfoPanelContainer.add(battleship2.getButton());
        }




    }

    /**
     * Metoda sprawdzająca czy w danym miejscu można postawic statek i blokująca pozostale mozliwosci
     * @param button - button startowy od ktorego rozpoczyna sie wyznaczanie mozliwosci postawienia statku

     */

    private void prepareToTyping(JButton button){

        String name = button.getName();
        int nameInInt = Integer.parseInt(name);
        int firstCord=intDecimal(nameInInt);
        int secondCord = intUnity(nameInInt);
        int mastNumber = selectedShip.getNumberOfMasts();
            mastNumber--;//zmieniszenie liczby masztów o ten postawiony w poprzednim ruchu
        for(int i =0;i<10;i++){
            for(int j=0;j<10;j++){
                JButton table= myButtonTable[i][j];
                table.setEnabled(false);
            }
        }
        boolean left = true;
        boolean right = true;
        boolean top = true;
        boolean bottom = true;

        for(int i=1;i<=mastNumber;i++){
          if(secondCord+mastNumber>=10||myBattleBoard[firstCord][secondCord+i]==1){

                right=false;
            }
            if(secondCord-mastNumber<0||myBattleBoard[firstCord][secondCord - i]==1) {
                left=false;

            }
            if(firstCord+mastNumber>=10|| myBattleBoard[firstCord + i][secondCord]==1) {
               bottom=false;
            }
            if(firstCord-mastNumber<0|| myBattleBoard[firstCord - i][secondCord]==1) {
               top=false;
            }

        }

        for(int i=1;i<=mastNumber;i++){
            if(right) {
                myButtonTable[firstCord][secondCord + i].setEnabled(true);
            }
            if(left) {
                myButtonTable[firstCord][secondCord - i].setEnabled(true);
            }
            if(bottom) {
                myButtonTable[firstCord + i][secondCord].setEnabled(true);
            }
            if(top) {
                myButtonTable[firstCord - i][secondCord].setEnabled(true);
            }
        }


    }

    /**
      * Metoda stawiająca statek na planszy gracza
     * @param button  - button startowy od ktorego zpopoczyna sie rozstawianie statku

     */
    private void shipTyping(JButton button){
        String name = button.getName();
        int nameInInt = Integer.parseInt(name);
        int lastButtonNumber = Integer.valueOf(lastBoardClickedButton);
        int course = nameInInt-lastButtonNumber;

       if(course>=10){
           //dol
           for (int i = intDecimal(lastButtonNumber); i <= (intDecimal(lastButtonNumber) + numberOfMastsOfSelectedShip); i++) {
               if(i == intDecimal(lastButtonNumber)){
                   setColumnShip(lastButtonNumber, i,1);
               }
               else{
                   if(i == (intDecimal(lastButtonNumber) + numberOfMastsOfSelectedShip)){
                       setColumnShip(lastButtonNumber, i,3);
                   }
                   else{
                       setColumnShip(lastButtonNumber, i,2);
                   }
               }

          }
           cleatAfterSetShip();

       }
       else{
           if(course<10&&course>0){
               //prawo
               for(int i = intUnity(lastButtonNumber);i<=(intUnity(lastButtonNumber)+numberOfMastsOfSelectedShip);i++){
                   if(i == intUnity(lastButtonNumber)){
                       setRowShip(lastButtonNumber,i,1);
                   }
                   else{
                       if(i==(intUnity(lastButtonNumber)+numberOfMastsOfSelectedShip)){
                           setRowShip(lastButtonNumber,i,3);
                       }
                       else {
                           setRowShip(lastButtonNumber, i, 2);
                       }
                   }

                }
                cleatAfterSetShip();
           }
           else{
               if(course>-10){
                   //lewo
                   for(int i = intUnity(lastButtonNumber);i>=(intUnity(lastButtonNumber)-numberOfMastsOfSelectedShip);i--){
                       if(i==intUnity(lastButtonNumber)){
                           setRowShip(lastButtonNumber, i, 3);
                       }
                       else {
                           if(i==(intUnity(lastButtonNumber)-numberOfMastsOfSelectedShip)){
                               setRowShip(lastButtonNumber, i, 1);
                           }
                           else {
                               setRowShip(lastButtonNumber, i, 2);
                           }
                           }
                    }
                    cleatAfterSetShip();
               }
               else{
                   //gora
                   for(int i = intDecimal(lastButtonNumber);i>=(intDecimal(lastButtonNumber)-numberOfMastsOfSelectedShip);i--) {
                       if( i == intDecimal(lastButtonNumber)){
                           setColumnShip(lastButtonNumber,i,3);
                       }
                       else{
                           if(i==(intDecimal(lastButtonNumber)-numberOfMastsOfSelectedShip)){
                               setColumnShip(lastButtonNumber,i,1);
                           }
                           else{
                               setColumnShip(lastButtonNumber,i,2);
                           }

                       }

                     }
                   cleatAfterSetShip();
               }
           }
       }

    }

    /**
     * Metoda obsługi klikniecia na planszę gracza , kliknieciu prszekierowuje ustawianie statku do metody  prepareToTyping(Jbutton button) pod warunkiem ze nie byl zaznaczony 1 masztowiec
     * @param button - aktualnie klikniety przycisk
     *
     */
    private void myButtonClick(JButton button){
        ImageIcon  ico = new ImageIcon(Class.class.getResource("/Battleships/image/oneMast.png"));

        if(numberOfMastsOfSelectedShip>0) {

            if(lastBoardClickedButton!=null&&selectedShip.getNumberOfMasts()>1) {

                shipTyping(button);
            }
            else{
                if(selectedShip.getNumberOfMasts()>1){

                    lastBoardClickedButton = button.getName();
                    numberOfMastsOfSelectedShip--;
                    int name = Integer.parseInt(button.getName());
                    myBattleBoard[intDecimal(name)][intUnity(name)]=1;
                    button.setBackground(Color.RED);
                    button.setIcon(ico);
                    button.setDisabledIcon(ico);
                    button.setEnabled(false);
                    prepareToTyping(button);
                }
                else{

                    button.setIcon(ico);
                    int name = Integer.parseInt(button.getName());
                    myBattleBoard[intDecimal(name)][intUnity(name)]=1;
                    button.setDisabledIcon(ico);
                    button.setBackground(Color.RED);
                    button.setEnabled(false);
                    cleatAfterSetShip();


                }

            }

        }
        else{
            JOptionPane.showConfirmDialog(null, "Wybierz kolejny statek do ustawienia", "Wybierz!", JOptionPane.CLOSED_OPTION);
            //#TODO wyświetl uzytkownikowi napis ("wybierz kolejny statek")
        }

    }
    /**
     * Metoda obsługi klikniecia na planszę przeciwnika  , Przyjmuje ona aktualnie klikniety button i sprawdza czy w danym miejscu jest statek przeciwnika czy nie i zmienia odpowiednio status przycisku oraz jego ikonę
     * @param button -  aktualnie klikniety button
     *
     */
    private void enemyButtonClick(JButton button){
           String name= button.getName();
           int nameInInt = Integer.parseInt(name);
           int firstCord = intDecimal(nameInInt);
           int secondCord = intUnity(nameInInt);
        ImageIcon icoHit = new ImageIcon(Class.class.getResource("/Battleships/image/hit.png"));
        ImageIcon icoMiss = new ImageIcon(Class.class.getResource("/Battleships/image/miss.png"));
           button.setEnabled(false);

           if(enemyBattleBoard[firstCord][secondCord]==1){
               enemyBattleBoard[firstCord][secondCord]=-1;
               button.setDisabledIcon(icoHit);
           }
           else {
               enemyBattleBoard[firstCord][secondCord] = 2;
               button.setDisabledIcon(icoMiss);
           }
           if(countOfSinkedShips(true)){
               System.out.println("GRACZ WYGRAL");
             winner(true);
               //TODO w tym miejscu dodac panel z wygrał gracz czy chcesz grac jeszcze raz
           }
           else{
               bot();
           }

    }

    /**
     * Metoda bota , działa ona w oparciu o liczby pseldolosowe , a w przypadku tafienia w staetk uruchamia metode theTargetWasHit() oraz pomija ruchy losowe do momentu wyczerpania przez nia mozliwisci
     *
     */
    private void bot (){

        if (lastBotShoot){
            theTargetWasHit();
        }
        else {
         //   System.out.println("RANDOM");
            Random generator = new Random();
            ImageIcon icoHit = new ImageIcon(Class.class.getResource("/Battleships/image/hit.png"));
            ImageIcon icoMiss = new ImageIcon(Class.class.getResource("/Battleships/image/miss.png"));
            int firstCord = generator.nextInt(10);
            int secondCord = generator.nextInt(10);
            if (myBattleBoard[firstCord][secondCord] != -1 && myBattleBoard[firstCord][secondCord] != 2) {
                if (myBattleBoard[firstCord][secondCord] == 1) {
                    myBattleBoard[firstCord][secondCord] = -1;
                    myButtonTable[firstCord][secondCord].setDisabledIcon(icoHit);
                    firstBotCord=firstCord;
                    secondBotCord=secondCord;
                   lastBotShoot=true;
                } else {
                    myBattleBoard[firstCord][secondCord] = 2;
                    myButtonTable[firstCord][secondCord].setDisabledIcon(icoMiss);
                }
                if (countOfSinkedShips(false)) {
                    System.out.println("KOMPUTER WYGRAL");
                  winner(false);

                }
            } else {
                bot();
            }
        }
    }
    /**
     * Metoda obsługujaca bota po trafieniu przez niego statku , po tafieniu bot zaczyna szukac statku najpierw w dol potem w gore , w prawo i w lewo jezeli statek "zostanie zatopiony" w pierwszym wyszukiwaniu (jakies pola zostały trafione podczas przeszukiwania w wertykalnego funkcja nie wywoluje wyszukiwania horyzontalnego) jest ona wywoływana przez metodę nadrzędna bota do wyxczerpania wszystkich mozliwosci (max 4)
     *
     */
    private void theTargetWasHit(){
        call++;
        ImageIcon icoHit = new ImageIcon(Class.class.getResource("/Battleships/image/hit.png"));
        ImageIcon icoMiss = new ImageIcon(Class.class.getResource("/Battleships/image/miss.png"));
        lastBotShoot=true;
     //   System.out.println("call = "+call+ "  side ="+side);
    switch (side){
        case 0:
            if(firstBotCord+call<10&&myBattleBoard[firstBotCord+call][secondBotCord]!=-1&&myBattleBoard[firstBotCord+call][secondBotCord]!=2) {
                if(call==2){
                    sank =true;
                }
                if (myBattleBoard[firstBotCord + call][secondBotCord] == 1) {
                    myBattleBoard[firstBotCord + call][secondBotCord] = -1;
                    myButtonTable[firstBotCord + call][secondBotCord].setDisabledIcon(icoHit);
                    myButtonTable[firstBotCord + call][secondBotCord].setEnabled(false);
                } else {
                    myBattleBoard[firstBotCord + call][secondBotCord] = 2;
                    myButtonTable[firstBotCord + call][secondBotCord].setDisabledIcon(icoMiss);
                    myButtonTable[firstBotCord + call][secondBotCord].setEnabled(false);
                    call = 0;
                    side++;
                }
            }
            else{
                call = 0;
                side++;
                theTargetWasHit();
            }
            break;
        case 1:
            if(firstBotCord-call>=0&&myBattleBoard[firstBotCord-call][secondBotCord]!=-1&&myBattleBoard[firstBotCord-call][secondBotCord]!=2) {
                if(call==2){
                    sank =true;
                }
                if (myBattleBoard[firstBotCord - call][secondBotCord] == 1) {
                    myBattleBoard[firstBotCord - call][secondBotCord] = -1;
                    myButtonTable[firstBotCord - call][secondBotCord].setDisabledIcon(icoHit);
                    myButtonTable[firstBotCord - call][secondBotCord].setEnabled(false);
                } else {
                    myBattleBoard[firstBotCord - call][secondBotCord] = 2;
                    myButtonTable[firstBotCord - call][secondBotCord].setDisabledIcon(icoMiss);
                    myButtonTable[firstBotCord - call][secondBotCord].setEnabled(false);
                    call = 0;
                    side++;
                }
            }
            else{
                call = 0;
                side++;
                theTargetWasHit();
            }
            break;
        case 2:
            if(sank){
                System.out.println("ship sank");
                lastBotShoot = false;
                sank =false;
                call = 0;
                side=0;
                bot();
            }
            else {
                if (secondBotCord + call < 10 && myBattleBoard[firstBotCord][secondBotCord + call] != -1 && myBattleBoard[firstBotCord][secondBotCord + call] != 2) {
                    if (myBattleBoard[firstBotCord][secondBotCord + call] == 1) {
                        myBattleBoard[firstBotCord][secondBotCord + call] = -1;
                        myButtonTable[firstBotCord][secondBotCord + call].setDisabledIcon(icoHit);
                        myButtonTable[firstBotCord][secondBotCord + call].setEnabled(false);
                    } else {
                        myBattleBoard[firstBotCord][secondBotCord + call] = 2;
                        myButtonTable[firstBotCord][secondBotCord + call].setDisabledIcon(icoMiss);
                        myButtonTable[firstBotCord][secondBotCord + call].setEnabled(false);
                        call = 0;
                        side++;
                    }
                } else {
                    call = 0;
                    side++;
                    theTargetWasHit();
                }
            }
            break;
        case 3:
            if(secondBotCord-call>=0&&myBattleBoard[firstBotCord][secondBotCord-call]!=-1&&myBattleBoard[firstBotCord][secondBotCord-call]!=2) {
                if (myBattleBoard[firstBotCord][secondBotCord - call] == 1) {
                    myBattleBoard[firstBotCord][secondBotCord - call] = -1;
                    myButtonTable[firstBotCord][secondBotCord - call].setDisabledIcon(icoHit);
                    myButtonTable[firstBotCord][secondBotCord - call].setEnabled(false);
                } else {
                    myBattleBoard[firstBotCord][secondBotCord - call] = 2;
                    myButtonTable[firstBotCord][secondBotCord - call].setDisabledIcon(icoMiss);
                    myButtonTable[firstBotCord][secondBotCord - call].setEnabled(false);
                    call = 0;
                    side = 0;
                    lastBotShoot = false;
                }
            }
            else {
                lastBotShoot = false;
                    call = 0;
                    side=0;
                    bot();
                }
            break;
            default:
                System.out.println("Nieoczekiwnay błąd"+side);
    }

    if(countOfSinkedShips(false)){
        System.out.println("KOMPUTER WYGRAL");
        winner(false);
    }
    }
    /**
     *
     * Metoda zliczajaca liczbe zatopionych statkow w zaleznosci od tego dla kogo została wywolanracz dla ktorego zostala wywolana zatopil wszystkie statki przeciwnika i zwracajaca true jesli odpowiedni gracz wygral

     * @param player -  wartośc  logiczna (true jezeli gracz ,  false jezeli komputer)
     * @return ileMasztowZostaloZatopionych

     */
    private boolean countOfSinkedShips(boolean player){
        boolean all =false;
        int count=0;
        int count2=0;
        if(player) {
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    if (enemyBattleBoard[i][j] == -1) {
                        count++;
                    }
                }
            }
        }
        else{
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    if (myBattleBoard[i][j] == -1) {
                        count2++;
                    }
                }
            }
        }
        System.out.println("Liczba zatopinych pól (20 to wygrana)"+count2);
       if(player&&count==20||!player&&count2==20){
            all=true;
        }
        return all;
    }

    /**
     * Metoda stawiajaca statek w pozycji pionowej zaleznie od miejsac zaczepienia statku oraz jego pozycji i czesci
     *
     * @param lastButtonNumber -  wspolrzedne ostatnio kliknietego bttona na planszy gracza
     *
     * @param i - iterator petli w ktorej wywolywana jest metoda
     * @param part - czesc statku (1 - dziub , 3 - rufa , 2 - cytadela)
     */
    private void setColumnShip(int lastButtonNumber , int i,int part ){
        ImageIcon ico ;
        if(part==1){
            ico = new ImageIcon(Class.class.getResource("/Battleships/image/prowCol.png"));
        }
        else {
            if(part==3){
                ico = new ImageIcon(Class.class.getResource("/Battleships/image/backCol.png"));
            }
            else {
                ico = new ImageIcon(Class.class.getResource("/Battleships/image/mp2Col.png"));
            }
        }
            myBattleBoard[i][intUnity(lastButtonNumber)]=1;
            JButton buttons =  myButtonTable[i][intUnity(lastButtonNumber)];
            buttons.setBorderPainted(false);
            buttons.setBackground(Color.RED);
            buttons.setIcon(ico);
            buttons.setDisabledIcon(ico);
            buttons.setEnabled(false);
    }
    /**
     *  Metoda stawiajaca statek w pozycji poziomej zaleznie od miejsac zaczepienia statku oraz jego pozycji i czesci
     *
     * @param lastButtonNumber - wspolrzedne ostatnio kliknietego bttona na planszy gracza
     * @param i - iterator petli w ktorej wywolywana jest metoda
     * @param part - czesc statku (1 - dziub , 3 - rufa , 2 - cytadela)

     */
    private void setRowShip(int lastButtonNumber , int i,int part ){
        ImageIcon ico;
        if(part==1){
            ico = new ImageIcon(Class.class.getResource("/Battleships/image/prow.png"));
        }
        else {
            if(part==3){
                ico = new ImageIcon(Class.class.getResource("/Battleships/image/back.png"));
            }
            else {
                ico = new ImageIcon(Class.class.getResource("/Battleships/image/mp2.png"));
            }
            }
        myBattleBoard[intDecimal(lastButtonNumber)][i]=1;
        JButton buttons =  myButtonTable[intDecimal(lastButtonNumber)][i];
        buttons.setIcon(ico);
        buttons.setBorderPainted(false);
        buttons.setDisabledIcon(ico);
        buttons.setBackground(Color.RED);
        buttons.setEnabled(false);
    }


    /**
     * Metoda zwracajaca dziesietna czesc liczby dwucyfrowej podanej w formacie int
     * @param number - liczba całkowita dwucyfrowa.
     * @return decimal  - dziesiętna część liczby całkowitej dwucyfrowej
     */
    public int intDecimal(int number){
        int decimal = (number/10);
        return decimal;
    }
    /**
     * Funkcja zwracajaca jednostkowa czesc liczby dwucyfrowej podanej w formacie int
     * @param number -liczba całkowita dwucyfrowa.
     * @return unity - jednostkowa czesc liczby całkowitej dwucyfrowek
     *
     */
    public int intUnity(int number){
        int unity = intDecimal(number)*10;
        unity = (number - unity);
        return unity;
    }

    /**
     * Metoda zerująca wartosi zmiennych globalnych przechowujacych aktualnie wybrany statek i ostatnio klikniety przycisk na planszy graczaoraz liczbe masztow statku ,i sprawdzajaca czy wszystkie statki zostały rozlokowane
     *         selectedShip=null;
     *         lastBoardClickedButton=null;
     *         numberOfMastsOfSelectedShip=0;
     */
    private void  cleatAfterSetShip(){
        selectedShip=null;
        lastBoardClickedButton=null;
        numberOfMastsOfSelectedShip=0;

        int countOfLocatedShip = 0;
        for (int i=0;i<10;i++){
            for(int j=0;j<10;j++){
                if(myBattleBoard[i][j]==1){
                    countOfLocatedShip++;
                }
                if(myBattleBoard[i][j]==0){
                    myButtonTable[i][j].setEnabled(true);
                }
            }
        }

    if(countOfLocatedShip==20){
        allShipsWhereLocated();
    }
    }
    /**
     * Metoda blokujaca mozliwośc ruchu na planszy gracza po rozmieszczeniu statkow i wywyłujaca metode rozmieszcajaca statki przeciwnika
     *
     */
    private void allShipsWhereLocated(){
        enemyBoard.setVisible(true);
        for(int i =0 ;i<10;i++){
            for(int j=0;j<10;j++){
                if(myBattleBoard[i][j]==0){
                    ImageIcon ico =  new ImageIcon(Class.class.getResource("/Battleships/image/water2.png"));
                    myButtonTable[i][j].setDisabledIcon(ico);
                    myButtonTable[i][j].setEnabled(false);
                }
            }
        }
        enemyShipsSet();
    }
    /**
     * Metoda Rozstawiająca statki przeciwnika
     */
    private void enemyShipsSet(){
        setOneEnemyShip(4);

        setOneEnemyShip(3);
        setOneEnemyShip(3);

        setOneEnemyShip(2);
        setOneEnemyShip(2);
        setOneEnemyShip(2);

        setOneEnemyShip(1);
        setOneEnemyShip(1);
        setOneEnemyShip(1);
        setOneEnemyShip(1);

       //showEnemyShips();

    }
    /**
     * Metoda rozstawiajaca pojedynczy statek przeciwnika w zaleznosci od liczby masztow statku
     * @param numberOfMasts - liczba masztow aktualnie wybranego statku(int);
     */
    private void setOneEnemyShip(int numberOfMasts){
        Random generator = new Random();
        int firstCord = generator.nextInt(10);
        int secondCord = generator.nextInt(10);
        int course = generator.nextInt(4);
        boolean isFree=true;
        switch (course){
            case 0 :
                for (int i =0;i<numberOfMasts;i++){
                    if(secondCord+i>=10||enemyBattleBoard[firstCord][secondCord+i]==1){
                     isFree=false;
                    }
                }

                if(isFree){
                    for (int i =0;i<numberOfMasts;i++){
                       enemyBattleBoard[firstCord][secondCord+i]=1;
                    }

                }
                else{
                    setOneEnemyShip(numberOfMasts);
                }

                break;
            case 1:
                for (int i =0;i<numberOfMasts;i++){
                    if(secondCord-i<0||enemyBattleBoard[firstCord][secondCord-i]==1){
                        isFree=false;
                    }
                }

                if(isFree){
                    for (int i =0;i<numberOfMasts;i++){
                        enemyBattleBoard[firstCord][secondCord-i]=1;
                    }

                }
                else{
                    setOneEnemyShip(numberOfMasts);
                }
                break;
            case 2:
                for (int i =0;i<numberOfMasts;i++){
                    if(firstCord+i>=10||enemyBattleBoard[firstCord+i][secondCord]==1){
                        isFree=false;
                    }
                }

                if(isFree){
                    for (int i =0;i<numberOfMasts;i++){
                        enemyBattleBoard[firstCord+i][secondCord]=1;
                    }

                }
                else{
                    setOneEnemyShip(numberOfMasts);
                }

                break;
            case 3:
                for (int i =0;i<numberOfMasts;i++){
                    if(firstCord-i<0||enemyBattleBoard[firstCord-i][secondCord]==1){
                        isFree=false;
                    }
                }

                if(isFree){
                    for (int i =0;i<numberOfMasts;i++){
                        enemyBattleBoard[firstCord-i][secondCord]=1;
                    }

                }
                else{
                    setOneEnemyShip(numberOfMasts);
                }

                break;

                default:
                    System.out.println("Działanie niepoprawne kurs róny = " +course );

        }


    }

    /**
     * Metoda tymczasowa potrzebna do sprawdzenia rozmieszczenia statkow podczas procesu tworzenia gry
     *
     */
    private void showEnemyShips(){
        for(int i = 0 ;i<10;i++){
            for(int j=0;j<10;j++){
                if(enemyBattleBoard[i][j]==1){
                    ImageIcon ico = new ImageIcon(Class.class.getResource("/Battleships/image/oneMast.png"));
                    enemyButtonTable[i][j].setIcon(ico);
                }
            }
        }

    }

    /**
     * * Metoda inicjalizujaca aktualnie klikniety statek i liczbe jego masztow
     *
     * @param ship - aktualnie klikniety staetek
     */
    public void setSelectedShip(Ship ship){
        if(selectedShip==null) {
            selectedShip = ship;
            selectedShip.getButton().setEnabled(false);
            numberOfMastsOfSelectedShip = selectedShip.getNumberOfMasts();
        }

    }

    /**
     * Metoda zwracając wielkośc pojedynczej komorki na planszy (getter)
     * @return boardSize -  wielkosc pojedynczej komorki n aplanszy stała dla całego pakietu
     *
     */
    public int getBoardSize(){
        return boardSize;
    }


    /**
     *Metoda tworzaca ekran zakonczenia gry w zaleznosci od tego czy wygrał gracz czy komputer
     * @param player - zmienna logiczna (true jesli gracz ,false jesli komuter)

     */

    private void winner (boolean player){

        turnOffAll();
        Start.frameWygrany.setContentPane(new Winner2(player).getPanel());
        Start.frameWygrany.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Start.frameWygrany.pack();
        Start.frameWygrany.setVisible(true);
        Start.frameWygrany.setLocationRelativeTo(null);
        Start.frameWygrany.setResizable(false);
        Start.frameWygrany.setMinimumSize(new Dimension(600,640));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
        Point newLocation = new Point(middle.x - (Start.frameWygrany.getWidth() / 2),
                middle.y - (Start.frameWygrany.getHeight() / 2));
        Start.frameWygrany.setLocation(newLocation);
    }
    /**
     *Metoda zwracająca uchwyt (panelGlowny)(getter)
     * @return mainPanel - glowny panel battleboards
     */
    public JPanel getMainPanel(){
        return mainPanel;
    }
    /**
     *Metoda ustawiajaca obie plansze na niemozliwe do klikniecia po zakonczeniu gry
     */
    public void turnOffAll(){

        for(int i =0 ; i<10;i++){
            for(int j=0;j<10;j++){
                myButtonTable[i][j].setEnabled(false);
                enemyButtonTable[i][j].setEnabled(false);

            }
        }
        buttonBack.setEnabled(false);
    }

}

